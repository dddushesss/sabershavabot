using Telegram.Bot;
using Telegram.Bot.Polling;
using Telegram.Bot.Types.Enums;

namespace ShavaBot;

public class BotController(ILogger<BotController> logger, IUpdateHandler service, ITelegramBotClient botClient)
    : IHostedService
{
    public Task StartAsync(CancellationToken cancellationToken)
    {
        var receiverOptions = new ReceiverOptions()
        {
            AllowedUpdates = new[] {UpdateType.Message, UpdateType.CallbackQuery}
        };
        logger.LogInformation("Bot Started receiving");
        botClient.StartReceiving(service, receiverOptions, cancellationToken);
        return Task.CompletedTask;
    }

    public Task StopAsync(CancellationToken cancellationToken)
    {
        return botClient.CloseAsync(cancellationToken);

    }
}