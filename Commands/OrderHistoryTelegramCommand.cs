﻿using CookBook.KeyboardHelper;
using CookBook.TelegramCommands;
using ShavaBot.TelegramCommands;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;

namespace ShavaBot.Commands;

[Command("orderhistory", TelegramCommandType.Message, "Вывести последние заказы")]
public class OrderHistoryTelegramCommand(DataBaseService dataBaseService) : ITelegramCommand
{
    public async Task HandleOnReceived(ITelegramBotClient botClient, Update update, CancellationToken cancellationToken)
    {
        if (update.Message?.From?.Username == null)
            return;

        var commandString =
            $"select Product, Meat, Sauce, Size, Toppings from OrderHistory where Nick = '@{update.Message.From.Username}'";
        var ordersString = await dataBaseService.ExecuteCommand(commandString, reader =>
        {
            var resultString = "";
            while (reader.Read())
            {
                for (var i = 0; i < reader.FieldCount; i++)
                {
                    var resultStrings = reader.GetString(i).Trim().Split('+');
                    resultString = resultStrings.Aggregate(resultString, (current, s) => current + s + "|");
                }

                resultString += "\n";
            }

            return resultString;
        });

        if (string.IsNullOrEmpty(ordersString))
            return;

        var keys = KeyboardHelper.CreateKeyboard<KeyboardButton>(ordersString.Split("\n"), 0);

        await botClient.SendTextMessageAsync(update.Message.Chat, "Предыдущие заказы: ",
            replyMarkup: new ReplyKeyboardMarkup(keys), cancellationToken: cancellationToken);
    }
}