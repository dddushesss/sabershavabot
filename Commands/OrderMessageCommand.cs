﻿using CookBook.TelegramCommands;
using ShavaBot.Order;
using ShavaBot.TelegramCommands;
using Telegram.Bot;
using Telegram.Bot.Types;

namespace ShavaBot.Commands;

[Command("ordermessage", TelegramCommandType.Message, "Вывести сообщение для заказа")]
public class OrderMessageCommand(OrderService orderService) : ITelegramCommand
{
    public async Task HandleOnReceived(ITelegramBotClient botClient, Update update,
        CancellationToken cancellationToken)
    {
        if (update.Message == null)
            return;
        
        await botClient.SendTextMessageAsync(update.Message.Chat, await orderService.GetOrderMessage(cancellationToken), cancellationToken: cancellationToken);
    }
}

[Command("ordertable", TelegramCommandType.Message, "Вывести текущие заказы с никами")]
public class OrderTableCommand(OrderService orderService) : ITelegramCommand
{
    public async Task HandleOnReceived(ITelegramBotClient botClient, Update update,
        CancellationToken cancellationToken)
    {
        if (update.Message == null)
            return;
        
        await botClient.SendTextMessageAsync(update.Message.Chat, await orderService.GetOrderTable(cancellationToken), cancellationToken: cancellationToken);
    }
}