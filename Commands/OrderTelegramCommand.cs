﻿using CookBook.KeyboardHelper;
using CookBook.TelegramCommands;
using ShavaBot.Data;
using ShavaBot.Order;
using ShavaBot.TelegramCommands;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;

namespace ShavaBot.Commands;

[Command("order", TelegramCommandType.Message, "Создать новый заказ шавухи")]
public class OrderTelegramCommand(OrderService orderService, TelegramCallbacksData telegramCallbacksData) : ITelegramCommand
{
    public async Task HandleOnReceived(ITelegramBotClient botClient, Update update, CancellationToken cancellationToken)
    {
        if (update.Message?.From?.Username == null || telegramCallbacksData.Reset == null || telegramCallbacksData.Order == null)
            return;

        InlineKeyboardMarkup resetInlineKeyboard = new(KeyboardHelper.CreateKeyboard<InlineKeyboardButton>(new[] {telegramCallbacksData.Reset}, 0));

        var order = await orderService.GetOrder(update.Message.From.Username, cancellationToken);
        
        if (order == null)
        {
            await botClient.SendTextMessageAsync(update.Message.Chat,
                $"Заказ уже произведён. Текущий заказ: {await orderService.GetTodayOrder(update.Message.From.Username, cancellationToken)}", replyMarkup: resetInlineKeyboard,
                cancellationToken: cancellationToken);
            return;
        }
        await order.ProcessOrder("", cancellationToken);

        var keyStrings = await order.GetCurrentRequests(cancellationToken);
        var keys = KeyboardHelper.CreateKeyboard<InlineKeyboardButton>(keyStrings, 1);
        keys.Add([InlineKeyboardButton.WithCallbackData(telegramCallbacksData.Reset)]);
        
        if (order.IsFilled())
        {
            keys.Add([InlineKeyboardButton.WithCallbackData(telegramCallbacksData.Order)]);
        }

        await botClient.SendTextMessageAsync(update.Message.Chat, $"{order}",
            cancellationToken: cancellationToken, replyMarkup: new InlineKeyboardMarkup(keys));
    }
}