﻿using CookBook.TelegramCommands;
using ShavaBot.Data;
using ShavaBot.Order;
using ShavaBot.TelegramCommands;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;

namespace ShavaBot.Commands;

[Command("Заказать", TelegramCommandType.CallbackQuery)]
public class PlaceOrderTelegramCommand(OrderService orderService, TelegramMessagesData messagesData) : ITelegramCommand
{
    public async Task HandleOnReceived(ITelegramBotClient botClient, Update update, CancellationToken cancellationToken)
    {
        if (update.CallbackQuery?.From.Username == null || update.CallbackQuery.Message == null)
        {
            return;
        }

        var order = await orderService.GetOrder(update.CallbackQuery.From.Username, cancellationToken);
        
        if (await orderService.TryPlaceOrder(update.CallbackQuery.From.Username, cancellationToken) && order != null)
        {
            var message = messagesData.GetOrderCompletedMessage(order);
            InlineKeyboardMarkup resetInlineKeyboard = new(new[]
            {
                new[]
                {
                    InlineKeyboardButton.WithCallbackData("Сбросить")
                }
            });
            await botClient.EditMessageTextAsync(update.CallbackQuery.Message.Chat.Id, update.CallbackQuery.Message.MessageId,
                message, replyMarkup: resetInlineKeyboard,
                parseMode: ParseMode.Markdown,
                cancellationToken: cancellationToken);
        }
    }
}