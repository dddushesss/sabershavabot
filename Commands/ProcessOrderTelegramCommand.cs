﻿using CookBook.KeyboardHelper;
using CookBook.TelegramCommands;
using ShavaBot.Data;
using ShavaBot.Order;
using ShavaBot.TelegramCommands;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;

namespace ShavaBot.Commands;

[Command(CommandServiceUpdateHandler.OtherCommandName, TelegramCommandType.CallbackQuery)]
public class ProcessOrderTelegramCommand(OrderService orderService, TelegramCallbacksData callbacksData)
    : ITelegramCommand
{
    public async Task HandleOnReceived(ITelegramBotClient botClient, Update update, CancellationToken cancellationToken)
    {
        if (update.CallbackQuery?.From.Username == null || update.CallbackQuery?.Message == null ||
            update.CallbackQuery.Data == null || callbacksData.Reset == null || callbacksData.Order == null)
            return;

        var order = await orderService.GetOrder(update.CallbackQuery.From.Username, cancellationToken);
        if (order == null)
            return;

        await order.ProcessOrder(update.CallbackQuery.Data, cancellationToken);

        var keyStrings = (await order.GetCurrentRequests(cancellationToken)).ToList();

        var keys = KeyboardHelper.CreateKeyboard<InlineKeyboardButton>(keyStrings.ToArray(), 1);
        keys.Add([InlineKeyboardButton.WithCallbackData(callbacksData.Reset)]);

        if (order.IsFilled())
            keys.Add([InlineKeyboardButton.WithCallbackData(callbacksData.Order)]);

        await botClient.EditMessageTextAsync(update.CallbackQuery.Message.Chat.Id,
            update.CallbackQuery.Message.MessageId, order.ToString(), replyMarkup: new InlineKeyboardMarkup(keys),
            cancellationToken: cancellationToken);
    }
}

[Command(CommandServiceUpdateHandler.OtherCommandName, TelegramCommandType.Message)]
public class ProcessOrderTelegramCommandMessage(OrderService orderService, TelegramCallbacksData callbacksData)
    : ITelegramCommand
{
    public async Task HandleOnReceived(ITelegramBotClient botClient, Update update, CancellationToken cancellationToken)
    {
        if (update.Message?.From?.Username == null || update.Message.Text == null ||
            callbacksData.Reset == null || callbacksData.Order == null)
            return;

        var order = await orderService.GetOrder(update.Message.From.Username, cancellationToken);
        if (order == null)
            return;

        await order.ProcessOrder(update.Message.Text, cancellationToken);

        var keyStrings = (await order.GetCurrentRequests(cancellationToken)).ToList();

        var keys = KeyboardHelper.CreateKeyboard<InlineKeyboardButton>(keyStrings.ToArray(), 1);
        keys.Add([InlineKeyboardButton.WithCallbackData(callbacksData.Reset)]);

        if (order.IsFilled())
            keys.Add([InlineKeyboardButton.WithCallbackData(callbacksData.Order)]);

        keyStrings.Add(callbacksData.Reset);
        if (order.IsFilled())
            keyStrings.Add(callbacksData.Order);

        await botClient.SendTextMessageAsync(update.Message.Chat.Id,
            order.ToString(), replyMarkup: new InlineKeyboardMarkup(keys),
            cancellationToken: cancellationToken);
    }
}