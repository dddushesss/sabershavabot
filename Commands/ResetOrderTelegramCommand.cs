﻿using CookBook.KeyboardHelper;
using CookBook.TelegramCommands;
using ShavaBot.Data;
using ShavaBot.Order;
using ShavaBot.TelegramCommands;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;

namespace ShavaBot.Commands;

[Command("Сбросить", TelegramCommandType.CallbackQuery)]
public class ResetOrderTelegramCommand(
    OrderService orderService,
    TelegramMessagesData messagesData,
    TelegramCallbacksData callbacksData) : ITelegramCommand
{
    public async Task HandleOnReceived(ITelegramBotClient botClient, Update update, CancellationToken cancellationToken)
    {
        if (update.CallbackQuery?.From.Username == null)
        {
            return;
        }

        var order = await orderService.GetOrder(update.CallbackQuery.From.Username, cancellationToken);

        if (order != null)
        {
            order.Reset();
        }
        else
        {
            await orderService.ResetTodayUsersOrder(update.CallbackQuery.From.Username);
        }
        
        await ProcessOrder(botClient, update, cancellationToken);
    }

    private async Task ProcessOrder(ITelegramBotClient botClient, Update update, CancellationToken cancellationToken)
    {
        if (update.CallbackQuery?.From.Username == null || update.CallbackQuery?.Message == null ||
            update.CallbackQuery.Data == null || callbacksData.Reset == null || callbacksData.Order == null)
            return;

        var order = await orderService.GetOrder(update.CallbackQuery.From.Username, cancellationToken);
        if (order == null)
            return;

        await order.ProcessOrder(update.CallbackQuery.Data, cancellationToken);

        var keyStrings = (await order.GetCurrentRequests(cancellationToken)).ToList();
        if (order.IsFilled())
            keyStrings.Add(callbacksData.Order);

        var keys = KeyboardHelper.CreateKeyboard<InlineKeyboardButton>(keyStrings.ToArray(), 1);
        keys.Add([InlineKeyboardButton.WithCallbackData(callbacksData.Reset)]);
        
        if (order.IsFilled())
            keys.Add([InlineKeyboardButton.WithCallbackData(callbacksData.Order)]);
        
        await botClient.EditMessageTextAsync(update.CallbackQuery.Message.Chat.Id,
            update.CallbackQuery.Message.MessageId, $"~{update.CallbackQuery.Message.Text}~", replyMarkup: new InlineKeyboardMarkup(keys),
            cancellationToken: cancellationToken);
        
        await botClient.EditMessageTextAsync(update.CallbackQuery.Message.Chat.Id,
            update.CallbackQuery.Message.MessageId, order.ToString(), replyMarkup: new InlineKeyboardMarkup(keys),
            cancellationToken: cancellationToken);
    }
}

[Command("reset", TelegramCommandType.Message, "Сбросить текущий заказ")]
public class ResetOrderTelegramCommandCallback(
    OrderService orderService,
    TelegramMessagesData messagesData,
    TelegramCallbacksData callbacksData)
    : ResetOrderTelegramCommand(orderService, messagesData, callbacksData);