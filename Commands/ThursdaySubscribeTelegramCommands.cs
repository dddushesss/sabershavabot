﻿using CookBook.KeyboardHelper;
using CookBook.TelegramCommands;
using ShavaBot.Data;
using ShavaBot.TelegramCommands;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;

namespace ShavaBot.Commands;

[Command("subscription", TelegramCommandType.Message, "Проверить статус подписки")]
public class ThursdaySubscribeCheckTelegramCommand(
    TelegramMessagesData telegramMessagesData,
    TelegramCallbacksData telegramCallbacksData,
    DataBaseService dataBaseService) : ITelegramCommand
{
    public async Task HandleOnReceived(ITelegramBotClient botClient, Update update, CancellationToken cancellationToken)
    {
        if (telegramCallbacksData.Unsubscribe == null || telegramCallbacksData.Subscribe == null ||
            telegramMessagesData.SubscriptionOn == null ||
            telegramMessagesData.SubscriptionOff == null || update.Message?.Chat.Id == null)
        {
            return;
        }

        var subscribersString = await dataBaseService.ExecuteCommand("SELECT * FROM ThursdaySubscribers", reader =>
        {
            var resultString = "";
            while (reader.Read())
            {
                resultString += reader.GetString(0) + "\n";
            }

            return resultString;
        });

        if (subscribersString == null)
        {
            return;
        }

        var subscribers = subscribersString.Split("\n");

        var key = telegramCallbacksData.Subscribe;
        var subscribeStatusMessage = telegramMessagesData.SubscriptionOff;

        if (subscribers.Contains(update.Message.Chat.Id.ToString()))
        {
            key = telegramCallbacksData.Unsubscribe;
            subscribeStatusMessage = telegramMessagesData.SubscriptionOn;
        }

        string[] keys = [key];
        var keyboard = KeyboardHelper.CreateKeyboard<InlineKeyboardButton>(keys, 0);
        await botClient.SendTextMessageAsync(update.Message.Chat, subscribeStatusMessage,
            replyMarkup: new InlineKeyboardMarkup(keyboard), cancellationToken: cancellationToken);
    }
}

[Command("Подписаться", TelegramCommandType.CallbackQuery)]
class ThursdaySubscribeTelegramCommand(
    DataBaseService dataBaseService,
    TelegramMessagesData telegramMessagesData,
    TelegramCallbacksData telegramCallbacksData
) : ITelegramCommand
{
    public async Task HandleOnReceived(ITelegramBotClient botClient, Update update, CancellationToken cancellationToken)
    {
        if (telegramCallbacksData.Unsubscribe == null || telegramCallbacksData.Subscribe == null ||
            telegramMessagesData.SubscriptionOn == null ||
            telegramMessagesData.SubscriptionOff == null || update.CallbackQuery?.Message?.Chat.Id == null)
        {
            return;
        }

        var chatId = update.CallbackQuery.Message.Chat.Id;

        var commandText = $"INSERT INTO ThursdaySubscribers VALUES('{chatId}')";
        await dataBaseService.ExecuteCommand(commandText);
        var keyboard = KeyboardHelper.CreateKeyboard<InlineKeyboardButton>([telegramCallbacksData.Unsubscribe], 0);
        await botClient.EditMessageTextAsync(update.CallbackQuery.Message.Chat.Id,
            update.CallbackQuery.Message.MessageId, telegramMessagesData.SubscriptionOn,
            replyMarkup: new InlineKeyboardMarkup(keyboard), cancellationToken: cancellationToken);
    }
}

[Command("Отписаться", TelegramCommandType.CallbackQuery)]
public class ThursdayUnsubscribeTelegramCommand(
    DataBaseService dataBaseService,
    TelegramMessagesData telegramMessagesData,
    TelegramCallbacksData telegramCallbacksData) : ITelegramCommand
{
    public async Task HandleOnReceived(ITelegramBotClient botClient, Update update,
        CancellationToken cancellationToken)
    {
        if (telegramCallbacksData.Unsubscribe == null || telegramCallbacksData.Subscribe == null ||
            telegramMessagesData.SubscriptionOn == null ||
            telegramMessagesData.SubscriptionOff == null || update.CallbackQuery?.Message?.Chat.Id == null)
        {
            return;
        }

        var chatId = update.CallbackQuery.Message.Chat.Id;

        var commandText = $"DELETE FROM ThursdaySubscribers WHERE nick = '{chatId}'";
        await dataBaseService.ExecuteCommand(commandText);
        var keyboard = KeyboardHelper.CreateKeyboard<InlineKeyboardButton>([telegramCallbacksData.Subscribe], 0);
        
        await botClient.EditMessageTextAsync(update.CallbackQuery.Message.Chat.Id,
            update.CallbackQuery.Message.MessageId, telegramMessagesData.SubscriptionOff,
            replyMarkup: new InlineKeyboardMarkup(keyboard), cancellationToken: cancellationToken);
    }
}