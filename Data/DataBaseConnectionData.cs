using MySql.Data.MySqlClient;

namespace ShavaBot.Data;

public class DataBaseConnectionData
{
    public string? ServerAddress { get; init; }
    public string? User { get; init; }
    public string? Password { get; init; }
    public string? DataBaseName { get; init; }

    public DataBaseConnectionData()
    {
    }

    public DataBaseConnectionData(IConfiguration configuration, ILogger<DataBaseConnectionData> logger)
    {
        var section = configuration.GetSection("DataBaseConnectionData");
        var newObject = section.Get<DataBaseConnectionData>();

        if (newObject == null)
        {
            logger.LogError("Can't read DataBaseConnectionData section from appsetting.json!");
            return;
        }

        ServerAddress = newObject.ServerAddress;
        User = newObject.User;
        Password = newObject.Password;
        DataBaseName = newObject.DataBaseName;
    }

    public override string ToString()
    {
        var connectionStringBuilder = new MySqlConnectionStringBuilder
        {
            Server = ServerAddress,
            Database = DataBaseName,
            UserID = User,
            Password = Password
        };
        return connectionStringBuilder.ConnectionString;
    }
}