﻿namespace ShavaBot.Data;

public class TelegramCallbacksData
{
    public string? Reset { get; init; }
    public string? Order { get; init; }
    public string? Subscribe { get; init; }
    public string? Unsubscribe { get; init; }

    public TelegramCallbacksData()
    {
    }

    public TelegramCallbacksData(IConfiguration configuration, ILogger<TelegramCallbacksData> logger)
    {
        var section = configuration.GetSection("TelegramCallbacks");
        var newObj = section.Get<TelegramCallbacksData>();

        if (newObj == null)
        {
            logger.LogError("Can't find TelegramCallbacks section in appsetting.data!");
            return;
        }
        
        Reset = newObj.Reset;
        Order = newObj.Order;
        Subscribe = newObj.Subscribe;
        Unsubscribe = newObj.Unsubscribe;
    }

}