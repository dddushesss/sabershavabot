﻿namespace ShavaBot.Data;

public class TelegramMessagesData
{
    public string? OrderReset { get; init; }
    public string? OrderAlreadyDone { get; init; }
    public string? OrderCompleted { get; init; }
    public string? SubscriptionOn { get; init; }
    public string? SubscriptionOff { get; init; }

    public TelegramMessagesData()
    {
    }

    public TelegramMessagesData(IConfiguration configuration, ILogger<TelegramCallbacksData> logger)
    {
        var section = configuration.GetSection("TelegramMessages");
        var newObject = section.Get<TelegramMessagesData>();

        if (newObject == null)
        {
            logger.LogError("Can't read TelegramMessages section from appsetting.json!");
            return;
        }

        OrderReset = newObject.OrderReset;
        OrderAlreadyDone = newObject.OrderAlreadyDone;
        OrderCompleted = newObject.OrderCompleted;
        SubscriptionOn = newObject.SubscriptionOn;
        SubscriptionOff = newObject.SubscriptionOff;
    }
    
    public string GetOrderCompletedMessage(Order.Order order)
    {
        if (OrderCompleted == null)
            return string.Empty;
        
        var result = OrderCompleted
            .Replace("{order}", order.GetShortString())
            .Replace("{prise}", order.GetPrise().ToString());
        return result;
    }
}