﻿namespace ShavaBot.Data;

public class ThursdayNotifyData
{
    public string? Cron { get; init; }
    public string? NotifySubscriber { get; init; }
    public int NotifyDelay { get; init; }
    public int SubscribersDataBaseName { get; init; }

    public ThursdayNotifyData()
    {
    }

    public ThursdayNotifyData(IConfiguration configuration, ILogger<ThursdayNotifyData> logger)
    {
        var section = configuration.GetSection("TelegramMessages");
        var newObject = section.Get<ThursdayNotifyData>();

        if (newObject == null)
        {
            logger.LogError("Can't read ThursdayNotifyData section from appsetting.json!");
            return;
        }

        Cron = newObject.Cron;
        NotifySubscriber = newObject.NotifySubscriber;
        NotifyDelay = newObject.NotifyDelay;
        SubscribersDataBaseName = newObject.SubscribersDataBaseName;
    }

    public string GetNotifyString(string nick)
    {
        return NotifySubscriber == null ? string.Empty : NotifySubscriber.Replace("{nick}", nick);
    }
}