﻿using MySql.Data.MySqlClient;
using ShavaBot.Data;

namespace ShavaBot;

public class DataBaseService(DataBaseConnectionData connectionData) : IDisposable
{
    private readonly MySqlConnection? _connection = new(connectionData.ToString());

    public async Task<string?> ExecuteCommand(string commandString, CbReaderToString? converter = null)
    {
        if (_connection == null)
            return null;
        
        await _connection.OpenAsync();
        var command = _connection.CreateCommand();
        command.CommandText = commandString;
        var reader = await command.ExecuteReaderAsync();
        var result = converter?.Invoke((MySqlDataReader) reader);
        await _connection.CloseAsync();
        return result;
    }

    public void Dispose()
    {
        _connection?.Close();
        _connection?.Dispose();
    }
}

public delegate string CbReaderToString(MySqlDataReader reader);