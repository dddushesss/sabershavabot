﻿FROM mcr.microsoft.com/dotnet/runtime:8.0 AS base
WORKDIR /app

FROM mcr.microsoft.com/dotnet/sdk:8.0 AS build
WORKDIR /src
COPY ["ShavaBot.csproj", "./"]
RUN dotnet restore "ShavaBot.csproj"
COPY . .
WORKDIR "/src/"
RUN dotnet build "ShavaBot.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "ShavaBot.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "ShavaBot.dll"]
