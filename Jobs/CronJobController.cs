﻿using Quartz;
using Quartz.Impl;
using Quartz.Simpl;

namespace ShavaBot.Jobs;

public class CronJobController
{
    public CronJobController(IEnumerable<ICronJobDesc> cronJobs)
    {
        var scheduler = StdSchedulerFactory.GetDefaultScheduler().Result;
        scheduler.JobFactory = new SimpleJobFactory();
        foreach (var cronJob in cronJobs)
        {
            var job = JobBuilder.Create<CronJob>()
                .WithIdentity(cronJob.GetIdentity().name, cronJob.GetIdentity().group)
                .Build();

            var trigger = TriggerBuilder.Create()
                .WithSchedule(CronScheduleBuilder.CronSchedule(cronJob.GetCronString()))
                .WithIdentity(cronJob.GetIdentity().name, cronJob.GetIdentity().group)
                .StartNow()
                .Build();

            scheduler.ScheduleJob(job, trigger);
        }
    }
}

public class CronJob(ICronJobDesc desc) : IJob
{
    public async Task Execute(IJobExecutionContext context)
    {
        await desc.Execute(context);
    }
}