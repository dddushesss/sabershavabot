﻿using Quartz;
using ShavaBot.Data;
using Telegram.Bot;

namespace ShavaBot.Jobs;

public interface ICronJobDesc
{
    public Task Execute(IJobExecutionContext context);
    public string GetCronString();
    public (string name, string group) GetIdentity();
}

public class ThursdayNotifyJobDescDesc(ThursdayNotifyData data, DataBaseService dataBaseService, ITelegramBotClient botClient) : ICronJobDesc
{
    public async Task Execute(IJobExecutionContext context)
    {
        var subscribers = new List<string>();
        await dataBaseService.ExecuteCommand("SELECT * FROM ThursdaySubscribers;", reader =>
        {
            while (reader.Read())
            {
                subscribers.Add(reader.GetString(0));
            }

            return "";
        });

        foreach (var subscriber in subscribers)
        {
            await Task.Delay(data.NotifyDelay);
            var chat = await botClient.GetChatAsync(subscriber);
            var nick = chat.FirstName + " " + chat.LastName;
            await botClient.SendTextMessageAsync(subscriber, data.GetNotifyString(nick),
                cancellationToken: context.CancellationToken);
        }
    }

    public string GetCronString()
    {
        return data.Cron ?? string.Empty;
    }

    public (string name, string group) GetIdentity()
    {
        return ("ThursdayNotify", "Notifiers");
    }
}