﻿using System.Text.RegularExpressions;
using MySql.Data.MySqlClient;

namespace ShavaBot.Order;

public enum OrderPositionType
{
    Type,
    Meat,
    Sauce,
    Size,
    Prise,
    Toppings
}

public class OrderPosition(bool isMultiFilled)
{
    public string Name = "";

    public void Fill(string value)
    {
        if (Name.Length > 0)
        {
            Name += " +";
        }

        Name += " " + value.Trim();
        Name = Name.Trim();
    }

    public bool IsFilled()
    {
        return !isMultiFilled && Name.Length > 0;
    }

    public override string ToString()
    {
        return Name;
    }
}

public class Order
{
    private readonly DataBaseService _dataBaseService;
    public OrderPosition this[OrderPositionType type] => _currentOrder[type];

    private readonly Dictionary<OrderPositionType, OrderPosition> _currentOrder =
        new(Enum.GetNames(typeof(OrderPositionType)).Length);

    public Order(DataBaseService dataBaseService)
    {
        _dataBaseService = dataBaseService;
        foreach (var typeName in Enum.GetNames(typeof(OrderPositionType)))
        {
            var type = Enum.Parse<OrderPositionType>(typeName);
            _currentOrder.Add(type, new OrderPosition(type == OrderPositionType.Toppings));
        }
    }

    public async Task ProcessOrder(string requests, CancellationToken cancellationToken)
    {
        var nextPositionTypes = _currentOrder
            .Where(order => !order.Value.IsFilled())
            .Select(order => order.Key)
            .ToArray();

        foreach (var request in requests.Split("|"))
        {
            foreach (var nextPositionType in nextPositionTypes)
            {
                var requestResult =
                    await GetStringArrayFromSql(GetSqlRequestByType(nextPositionType));

                if (!requestResult.Contains(request.Trim()) && requestResult.Length == 1 && nextPositionType != OrderPositionType.Toppings)
                    await ProcessOrder(requestResult[0], cancellationToken);
                
                if (!requestResult.Contains(request.Trim()) || _currentOrder[nextPositionType].IsFilled()) continue;

                _currentOrder[nextPositionType].Fill(request.Trim());

                var newResults = await GetCurrentRequests(cancellationToken);
                if(newResults.Length == 1 && nextPositionType != OrderPositionType.Toppings)
                    await ProcessOrder(requestResult[0], cancellationToken);
                
                break;
            }
        }
    }

    public void Reset()
    {
        foreach (var position in _currentOrder)
        {
            position.Value.Name = "";
        }
    }

    public async Task<string?[]> GetCurrentRequests(CancellationToken cancellationToken)
    {
        var lastFilled = _currentOrder.FirstOrDefault(order => !order.Value.IsFilled()).Key;
        var res = (await GetStringArrayFromSql(GetSqlRequestByType(lastFilled))).ToList();
        res.RemoveAll((str => _currentOrder[lastFilled].Name.Contains(str)));

        return res.ToArray();
    }

    public bool IsFilled()
    {
        return _currentOrder.ToList()
            .TrueForAll(order => order.Value.IsFilled() || order.Key == OrderPositionType.Toppings);
    }

    private async Task<string[]> GetStringArrayFromSql(string request)
    {
        var resultString = await _dataBaseService.ExecuteCommand(request, StringToStringArrayButtonConverter);
        return resultString?.Split("\n") ?? Array.Empty<string>();
    }

    private string StringToStringArrayButtonConverter(MySqlDataReader reader)
    {
        var resultString = "";

        while (reader.Read())
        {
            var keyName = "";
            for (var i = 0; i < reader.FieldCount; i++)
            {
                keyName = string.Join(" ", keyName, reader.GetValue(i).ToString());
            }

            resultString += keyName.Trim() + "\n";
        }

        return resultString.Trim();
    }

    private string GetSqlRequestByType(OrderPositionType type)
    {
        var meatRequest = _currentOrder[OrderPositionType.Type].IsFilled()
            ? $"and Product.Meat = '{_currentOrder[OrderPositionType.Meat]}'"
            : "";
        var sauceRequest = _currentOrder[OrderPositionType.Sauce].IsFilled()
            ? $"and Sauce.Name = '{_currentOrder[OrderPositionType.Sauce]}'"
            : "";
        var sizeRequest = _currentOrder[OrderPositionType.Size].IsFilled()
            ? $"and Size.Name = '{_currentOrder[OrderPositionType.Size]}'"
            : "";

        return type switch
        {
            OrderPositionType.Type => $"select distinct Product.Name from Product",
            OrderPositionType.Meat =>
                $"select distinct Meat from Product where Product.Name = '{_currentOrder[OrderPositionType.Type]}'",
            OrderPositionType.Sauce =>
                $"select distinct Sauce.Name from Sauce join Product on Product.Name = '{_currentOrder[OrderPositionType.Type]}'  {meatRequest}",
            OrderPositionType.Size => $"select distinct Size.Name from Size " +
                                      $" join Product on Size.Id = Product.SizeId and Product.Name = '{_currentOrder[OrderPositionType.Type]}' " +
                                      $" {meatRequest} join Sauce " +
                                      $" on Sauce.Id = Product.SauceId {sauceRequest}",
            OrderPositionType.Prise => $"select distinct Size.Prise from Size " +
                                       $" join Product on Size.Id = Product.SizeId and Product.Name = '{_currentOrder[OrderPositionType.Type]}' " +
                                       $"{sizeRequest} " +
                                       $"{meatRequest} join Sauce " +
                                       $"on Sauce.Id = Product.SauceId {sauceRequest}",
            OrderPositionType.Toppings =>
                $"select distinct Toppings.Name, Toppings.Prise from Toppings join Product on Toppings.Id = Product.ToppingsId " +
                $"and Product.Name = '{_currentOrder[OrderPositionType.Type]}' {meatRequest} " +
                $"join Sauce on Sauce.Id = Product.SauceId {sauceRequest} " +
                $"join Size on Product.SizeId = Size.Id {sizeRequest}",
            _ => throw new ArgumentOutOfRangeException(nameof(type), type, null)
        };
    }

    public string GetShortString()
    {
        return "Заказ: " + $"{_currentOrder[OrderPositionType.Type]} {_currentOrder[OrderPositionType.Meat]} " +
               $"{_currentOrder[OrderPositionType.Size]} {_currentOrder[OrderPositionType.Sauce]} {(_currentOrder[OrderPositionType.Sauce].IsFilled() ? "соус" : "")}" +
               $"{(_currentOrder[OrderPositionType.Toppings].Name.Length > 0 ? " + " + Regex.Replace(_currentOrder[OrderPositionType.Toppings].Name, "[0-9]", "").Trim() : "")}";
    }

    public int GetPrise()
    {
        if (!int.TryParse(_currentOrder[OrderPositionType.Prise].Name, out var basePrise))
            return 0;

        var toppingsStr = _currentOrder[OrderPositionType.Toppings].Name;

        if (toppingsStr.Length == 0)
            return basePrise;

        return toppingsStr.Replace("&", "")
            .Split(" ")
            .Select(str => int.TryParse(str, out var value) ? value : 0)
            .Sum() + basePrise;
    }

    public override string ToString()
    {
        return GetShortString() +
               (_currentOrder[OrderPositionType.Prise].IsFilled()
                   ? $"\nЦена: {GetPrise()}₽"
                   : "");
    }
}