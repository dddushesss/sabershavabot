﻿using MySql.Data.MySqlClient;

namespace ShavaBot.Order;

public class OrderService(DataBaseService dataBaseService)
{
    private readonly Dictionary<string, Order> _orders = new();

    public async Task<bool> TryPlaceOrder(string nick, CancellationToken cancellationToken)
    {
        var order = _orders.GetValueOrDefault(nick);
        if (order == null || !order.IsFilled())
        {
            return false;
        }

        await SaveToHistory(nick);
        _orders.Remove(nick);

        return true;
    }

    public async Task<Order?> GetOrder(string nick, CancellationToken cancellationToken)
    {
        var todayOrders = await TryGetTodayOrders(cancellationToken);
        var order = _orders.GetValueOrDefault(nick);

        if (order == null && todayOrders != null && !todayOrders.ContainsKey($"@{nick}"))
        {
            order = new Order(dataBaseService);
            _orders.Add(nick, order);
        }

        return order;
    }
    
    public async Task<Order?> GetTodayOrder(string nick, CancellationToken cancellationToken)
    {
        var todayOrders = await TryGetTodayOrders(cancellationToken);
        return todayOrders?.GetValueOrDefault($"@{nick}");
    }

    private async Task SaveToHistory(string nick)
    {
        var order = _orders[nick];

        if (!order.IsFilled())
            return;
        var data = DateTime.Now;

        await dataBaseService.ExecuteCommand(
            "INSERT INTO OrderHistory (Nick, Product, Meat, Sauce, Toppings, Size, Prise, Date, Time)" +
            $"VALUES ('@{nick}', '{order[OrderPositionType.Type]}', '{order[OrderPositionType.Meat]}', " +
            $"'{order[OrderPositionType.Sauce]}', '{order[OrderPositionType.Toppings]}', '{order[OrderPositionType.Size]}', " +
            $"{order[OrderPositionType.Prise]}, '{data.Date.ToShortDateString()}', '{data.ToShortTimeString()}')");
    }

    public async Task<string> GetOrderMessage(CancellationToken cancellationToken)
    {
        var orders = await TryGetTodayOrders(cancellationToken);
        if (orders == null)
            return "";

        var filledOrders = orders.Where(order => order.Value.IsFilled()).ToList();
        var orderString = $"Добрый день! Хотел бы заказать {filledOrders.Count} шаверм:";
        var orderDict = new Dictionary<string, int>();
        foreach (var order in filledOrders)
        {
            if (!orderDict.ContainsKey(order.Value.GetShortString()))
            {
                orderDict.Add(order.Value.GetShortString(), 1);
            }
            else
            {
                orderDict[order.Value.GetShortString()]++;
            }
        }

        return orderDict.Aggregate(orderString, (current, order) => current + $"\n{order.Value}X {order.Key}");
    }

    public async Task<string> GetOrderTable(CancellationToken cancellationToken)
    {
        var orders = await TryGetTodayOrders(cancellationToken);
        if (orders == null) return "";
        return orders.Aggregate("Кто, что и за сколько:",
            (current, order) => current + $"\n{order.Key} заказал {order.Value}\n");
    }

    private async Task<Dictionary<string, Order>?> TryGetTodayOrders(CancellationToken cancellationToken)
    {
        var data = DateTime.Now;
        var orders = new Dictionary<string, Order>();

        var commandText =
            $"select Nick, Product, Meat, Sauce, Size, Toppings from OrderHistory where Date = '{data.Date.ToShortDateString()}'";

        var resultString = await dataBaseService.ExecuteCommand(commandText, StringToOrderConverter);
        var orderStrings = resultString?.Split("\n");

        if (orderStrings == null)
            return null;
        var order = new Order(dataBaseService);

        for (var i = 1; i < orderStrings.Length; i++)
        {
            var orderString = orderStrings[i];
            string?[] splitedPositions = orderString.Split("+");
            foreach (var position in splitedPositions)
            {
                if (position == null) continue;
                await order.ProcessOrder(position, cancellationToken);
            }

            if (!order.IsFilled()) continue;
            orders.Add(orderStrings[0], order);
            order = new Order(dataBaseService);
        }

        return orders;
    }

    private string StringToOrderConverter(MySqlDataReader reader)
    {
        var resultStrings = "";
        while (reader.Read())
        {
            for (var i = 0; i < reader.FieldCount; i++)
            {
                resultStrings += reader.GetString(i) + "\n";
            }
        }

        return resultStrings.Trim();
    }

    public async Task ResetTodayUsersOrder(string username)
    {
        var data = DateTime.Now;

        var commandText =
            $"DELETE FROM OrderHistory WHERE Nick = '@{username}' and Date = '{data.ToShortDateString()}'";
        await dataBaseService.ExecuteCommand(commandText);
    }
}