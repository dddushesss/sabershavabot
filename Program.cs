using CookBook.TelegramCommands;
using Quartz;
using ShavaBot;
using ShavaBot.Commands;
using ShavaBot.Data;
using ShavaBot.Jobs;
using ShavaBot.Order;
using ShavaBot.TelegramCommands;
using Telegram.Bot;
using Telegram.Bot.Polling;

var builder = Host.CreateApplicationBuilder(args);
builder.Services.AddHostedService<BotController>();
builder.Services.AddSingleton<IUpdateHandler, CommandServiceUpdateHandler>();
builder.Services.AddSingleton<DataBaseService>();
builder.Services.AddSingleton<OrderService>();
builder.Services.AddTransient<ITelegramCommand, OrderTelegramCommand>();
builder.Services.AddTransient<ITelegramCommand, ProcessOrderTelegramCommand>();
builder.Services.AddTransient<ITelegramCommand, ProcessOrderTelegramCommandMessage>();
builder.Services.AddTransient<ITelegramCommand, OrderHistoryTelegramCommand>();
builder.Services.AddTransient<ITelegramCommand, ResetOrderTelegramCommand>();
builder.Services.AddTransient<ITelegramCommand, PlaceOrderTelegramCommand>();
builder.Services.AddTransient<ITelegramCommand, ResetOrderTelegramCommandCallback>();
builder.Services.AddTransient<ITelegramCommand, OrderMessageCommand>();
builder.Services.AddTransient<ITelegramCommand, OrderTableCommand>();
builder.Services.AddTransient<ITelegramCommand, ThursdaySubscribeCheckTelegramCommand>();
builder.Services.AddTransient<ITelegramCommand, ThursdaySubscribeTelegramCommand>();
builder.Services.AddTransient<ITelegramCommand, ThursdayUnsubscribeTelegramCommand>();
builder.Services.AddSingleton<ITelegramBotClient>(provider =>
    {
        var token = Environment.GetEnvironmentVariable("TELETOKEN");
        if (token == null)
            throw new InvalidOperationException("Can't get telegram token");
        var bot = new TelegramBotClient(token);
        if (provider.GetService<IUpdateHandler>() is CommandServiceUpdateHandler commandHandler)
            bot.SetMyCommandsAsync(commandHandler.GetCommandsDesc());
        
        return bot;
    }
);

builder.Services.AddSingleton<TelegramCallbacksData>();
builder.Services.AddTransient<ICronJobDesc, ThursdayNotifyJobDescDesc>();
builder.Services.AddTransient<ThursdayNotifyData>();
builder.Services.AddQuartz();
builder.Services.AddQuartzHostedService();
builder.Services.AddSingleton<TelegramMessagesData>();
builder.Services.AddSingleton<DataBaseConnectionData>();
builder.Services.AddSingleton<CronJobController>();
var host = builder.Build();
host.Run();