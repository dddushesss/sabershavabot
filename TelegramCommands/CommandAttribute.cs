namespace ShavaBot.TelegramCommands;

public enum TelegramCommandType
{
    All,
    Message,
    CallbackQuery
}

[AttributeUsage(AttributeTargets.Class, Inherited = false)]
class CommandAttribute : Attribute
{
    public string Name { get; }
    public TelegramCommandType Type { get; }
    public string Description { get; }

    public CommandAttribute(string name, TelegramCommandType type = TelegramCommandType.All, string description = "")
    {
        Name = name;
        Description = description;
        Type = type;
    }
}