﻿using System.Reflection;
using CookBook.TelegramCommands;
using Telegram.Bot;
using Telegram.Bot.Polling;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace ShavaBot.TelegramCommands;

public class CommandServiceUpdateHandler : IUpdateHandler, IDisposable
{
    public const string OtherCommandName = "%other";
    
    private readonly Dictionary<TelegramCommandType, Dictionary<string, List<ITelegramCommand>>?> _commands = new();
    private readonly Dictionary<TelegramCommandType, List<ITelegramCommand>> _anyCommands = new();
    private readonly ILogger _logger;
    private readonly List<(string name, string desc)> _commandsDesc = [];

    public CommandServiceUpdateHandler(ILogger<CommandServiceUpdateHandler> logger, IEnumerable<ITelegramCommand> commands)
    {
        foreach (TelegramCommandType type in Enum.GetValues(typeof(TelegramCommandType)))
        {
            _commands.Add(type, new Dictionary<string, List<ITelegramCommand>>());
            _anyCommands.Add(type, []);
        }
        _logger = logger;
        foreach (var command in commands)
        {
            RegisterCommand(command);
        }
    }

    private void RegisterCommand(ITelegramCommand command)
    {
        var attribute = command.GetType().GetCustomAttribute<CommandAttribute>();

        if (attribute != null)
        {
            var name = attribute.Name;
            var type = attribute.Type;

            if (name == OtherCommandName)
            {
                _anyCommands[type].Add(command);
                return;
            }

            var commandsByType = _commands[type];

            List<ITelegramCommand>? commandsByName = null;

            if (commandsByType != null && !commandsByType.TryGetValue(name, out commandsByName))
            {
                commandsByName = new List<ITelegramCommand>();
                commandsByType.Add(name, commandsByName);
            }

            commandsByName?.Add(command);
            if (type != TelegramCommandType.CallbackQuery)
            {
                _commandsDesc.Add((name, attribute.Description));
            }   
        }
        else
        {
            var className = command.GetType().Name;
            _logger.LogError($"{className} doesn't implemented attribute [Command]!");
        }
    }
    
    public IEnumerable<BotCommand> GetCommandsDesc()
    {
        return _commandsDesc.Where(desc => desc.desc.Length > 0).Select(desc => new BotCommand {Command = desc.name, Description = desc.desc});
    }

    public Task HandleUpdateAsync(ITelegramBotClient botClient, Update update, CancellationToken cancellationToken)
    {
        return update.Type switch
        {
            UpdateType.Message => HandleOnUpdated(TelegramCommandType.Message, botClient, update, cancellationToken),
            UpdateType.CallbackQuery => HandleOnUpdated(TelegramCommandType.CallbackQuery, botClient, update, cancellationToken),
            _ => Task.CompletedTask
        };
    }

    public Task HandlePollingErrorAsync(ITelegramBotClient botClient, Exception exception,
        CancellationToken cancellationToken)
    {
        _logger.LogError($"Error in bot client!\n{exception}");

        return Task.CompletedTask;
    }

    private Task HandleOnUpdated(TelegramCommandType commandType, ITelegramBotClient botClient, Update update,
        CancellationToken cancellationToken)
    {
        var commandName = GetCommandName(commandType, update);

        if (commandName.Length == 0)
            return Task.CompletedTask;

        var commands = GetCommandsByType(commandType, commandName);

        return Task.WhenAll(
            commands.Select(
                command => command.HandleOnReceived(botClient, update, cancellationToken)
            )
        );
    }

    private string GetCommandName(TelegramCommandType commandType, Update update)
    {
        switch (commandType)
        {
            case TelegramCommandType.All:
                switch (update.Type)
                {
                    case UpdateType.Message when update.Message?.Text != null:
                        return update.Message.Text.Replace("/", "");
                    case UpdateType.CallbackQuery when update.CallbackQuery?.Data != null:
                        return update.CallbackQuery.Data;
                    default:
                        _logger?.LogError($"Doesn't support {update.Type}");
                        break;
                }

                break;
            case TelegramCommandType.Message:
                if (update.Message?.Text != null)
                    return update.Message.Text.Replace("/", "");
                break;
            case TelegramCommandType.CallbackQuery:
                if (update.CallbackQuery?.Data != null)
                    return update.CallbackQuery.Data;
                break;
            default:
                return "";
        }

        return "";
    }

    private List<ITelegramCommand> GetCommandsByType(TelegramCommandType type, string message)
    {
        var commandList = new List<ITelegramCommand>();

        if (_commands.TryGetValue(type, out var commandsTyped) && commandsTyped != null &&
            commandsTyped.TryGetValue(message, out var typedCommandList))
        {
            commandList.AddRange(typedCommandList);
        }

        if (_commands.TryGetValue(TelegramCommandType.All, out var commandsAll) && commandsAll != null &&
            commandsAll.TryGetValue(message, out var allCommandList))
        {
            commandList.AddRange(allCommandList);
        }
        
        if (commandList.Count == 0)
            commandList.AddRange(_anyCommands[type]);

        return commandList;
    }

    public void Dispose()
    {
        foreach (var command in _commands)
        {
            (command.Value as IDisposable)?.Dispose();
        }
        _commands.Clear();
    }
}