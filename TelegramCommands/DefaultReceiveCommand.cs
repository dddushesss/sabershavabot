using CookBook.TelegramCommands;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;

namespace ShavaBot.TelegramCommands;

[Command("ping", TelegramCommandType.Message)]
class DefaultReceiveCommand : ITelegramCommand
{
    public Task HandleOnReceived(ITelegramBotClient botClient, Update update, CancellationToken cancellationToken)
    {
        if (update.Message?.Chat == null)
            return Task.CompletedTask;
        var keyboard = CookBook.KeyboardHelper.KeyboardHelper.CreateKeyboard<InlineKeyboardButton>(new[] {"ping"}, 1);
        return botClient.SendTextMessageAsync(update.Message.Chat, "Pong",
            replyMarkup: new InlineKeyboardMarkup(keyboard), cancellationToken: cancellationToken);
    }
}

[Command("ping", TelegramCommandType.CallbackQuery)]
class DefaultReceiveCommandQuery : ITelegramCommand
{
    public Task HandleOnReceived(ITelegramBotClient botClient, Update update, CancellationToken cancellationToken)
    {
        if (update.CallbackQuery?.Message == null)
            return Task.CompletedTask;
        return botClient.SendTextMessageAsync(update.CallbackQuery.Message.Chat, "Pong",
            cancellationToken: cancellationToken);
    }
}