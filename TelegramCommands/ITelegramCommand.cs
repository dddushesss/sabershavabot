using Telegram.Bot;
using Telegram.Bot.Types;

namespace CookBook.TelegramCommands;

public interface ITelegramCommand
{
    public Task HandleOnReceived(ITelegramBotClient botClient, Update update, CancellationToken cancellationToken);
}