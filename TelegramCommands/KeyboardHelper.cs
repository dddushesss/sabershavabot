﻿using Telegram.Bot.Types.ReplyMarkups;

namespace CookBook.KeyboardHelper;

public static class KeyboardHelper
{
    private static IKeyboardButton CreateKeyboardButton(string name)
    {
        return new KeyboardButton(name);
    }

    public static List<T[]> CreateKeyboard<T>(IEnumerable<string?> keysText, int lineCount) where T : IKeyboardButton
    {
        var keys = new List<T[]>();
        var line = new List<T>();
        var createButtonCb = CreateKeyboardButton;

        if (typeof(T) == typeof(InlineKeyboardButton))
        {
            createButtonCb = InlineKeyboardButton.WithCallbackData;
        }

        var i = 0;
        foreach (var text in keysText)
        {
            if (text == null) continue;
            
            var button = createButtonCb(text);
            line.Add((T) button);
            if (i++ < lineCount) continue;
            i = 0;
            keys.Add(line.ToArray());
            line = new List<T>();
        }

        if (line.Any())
        {
            keys.Add(line.ToArray());
        }

        return keys;
    }

}